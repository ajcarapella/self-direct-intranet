<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
  <meta name="description" content="<?php if ( is_single() ) {
      single_post_title('', true);
    } else {
      bloginfo('name'); echo " - "; bloginfo('description');
    }
  ?>" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php wp_head(); ?>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
</head>

<body <?php body_class(); ?>>

<nav class="navbar navbar-expand-lg navbar-light">
  <div class="container-responsive">
    <a class="navbar-brand" href="<?php echo esc_url( home_url('/') ); ?>"><img class="d-table w-100" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="<?php bloginfo( 'name' ); ?>"></a>

    <div class="collapse navbar-collapse" id="navbarDropdown">
      <?php
        wp_nav_menu( array(
          'theme_location'  => 'navbar',
          'container'       => false,
          'menu_class'      => '',
          'fallback_cb'     => '__return_false',
          'items_wrap'      => '<ul id="%1$s" class="navbar-nav mr-auto mt-2 mt-lg-0 %2$s">%3$s</ul>',
          'depth'           => 2,
          'walker'          => new b4st_walker_nav_menu()
        ) );
      ?>
    </div>
    
  </div>
</nav>
<div class="ad-bar">
	<div class="ad-bar__ad-container">
			<div class="user-info">
					<?php global $current_user; get_currentuserinfo();?>
					<span class="remove-mobile">Welcome back, </span><?php echo $current_user->user_firstname;?> <?php echo $current_user->user_lastname;?><span class="remove-mobile">!</span> <a href="<?php echo wp_logout_url(); ?>" class="logout-link">Logout</a><?php //echo bp_core_get_user_firstname( bp_loggedin_user_id() ); ?>
		    </div>
			<!--<div class="home-alerts">
				<a href="<?php echo home_url('/members/') . $current_user->user_nicename . '/notifications'; ?>"><strong>Notifications</strong>&nbsp;<div class="alert-blurb"><?php echo bp_notifications_get_unread_notification_count( $user_id );?></div></a>
			</div>-->
			<?php if( current_user_can('editor') || current_user_can('administrator') ) {  ?>
				<div class="admin-options">
				     <a href="<?php echo home_url('/'); ?>add-news">+ <span class="remove-mobile">Add News </span>Post</a>&nbsp;&nbsp;|&nbsp;&nbsp;
				     <a href="<?php echo home_url('/'); ?>add-user">+ <span class="remove-mobile">Add </span>User</a>
				</div>
			<?php } ?>
			
	     
	 </div>
 </div>
