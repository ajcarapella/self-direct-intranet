<?php

// REGISTER CUSTOM COMPONENTS
function bp_register_custom_components($component_names = array()) {

  // Force $component_names to be an array
  if (! is_array($component_names)) {
    $component_names = array();
  }

  // Add custom components to registered components array
  array_push($component_names, 'alert-comp');
  array_push($component_names, 'corporatenews-comp');
  array_push($component_names, 'shoutout-comp');

  // Return components with custom components appended
  return $component_names;
}
add_filter('bp_notifications_get_registered_components', 'bp_register_custom_components');




// FORMAT CUSTOM COMPONENT NOTIFICATIONS
function bp_custom_components_format_notif($action, $item_id, $secondary_item_id, $total_items, $format = 'string') {


  // ALERT CPT
  if ($action === 'alert-posted') {

    $post_info = get_post($item_id);

    $custom_title = get_post_meta($item_id, 'headline', true);
    $custom_text  = get_post_meta($item_id, 'headline', true);
    $custom_link  = get_post_permalink($item_id);

    // WordPress Toolbar
    if ('string' === $format) {
      $return = apply_filters('custom_filter', '' . esc_html($custom_text) . '', $custom_text, $custom_link);

    // Deprecated BuddyBar
    } else {
      $return = apply_filters('custom_filter', array(
        'text' => $custom_text,
        'link' => $custom_link
     ), $custom_link, (int) $total_items, $custom_text, $custom_title);
    }

    return $return;
  }


  // CORPORATE NEWS
  if ($action === 'corporatenews-posted') {

    $post_info = get_post($item_id);

    $custom_title = get_post_meta($item_id, 'headline', true);
    $custom_text  = get_post_meta($item_id, 'headline', true);
    $custom_link  = get_post_permalink($item_id);

    // WordPress Toolbar
    if ('string' === $format) {
      $return = apply_filters('custom_filter', '' . esc_html($custom_text) . '', $custom_text, $custom_link);

    // Deprecated BuddyBar
    } else {
      $return = apply_filters('custom_filter', array(
        'text' => $custom_text,
        'link' => $custom_link
     ), $custom_link, (int) $total_items, $custom_text, $custom_title);
    }

    return $return;
  }


  // SHOUTOUTS
  if ($action === 'shoutout-posted') {

    $post_info = get_post($item_id);

    $custom_title = get_post_meta($item_id, 'so_headline', true);
    $custom_text  = get_post_meta($item_id, 'so_headline', true);
    $custom_link  = get_post_permalink($item_id);

    // WordPress Toolbar
    if ('string' === $format) {
      $return = apply_filters('custom_filter', '' . esc_html($custom_text) . '', $custom_text, $custom_link);

    // Deprecated BuddyBar
    } else {
      $return = apply_filters('custom_filter', array(
        'text' => $custom_text,
        'link' => $custom_link
     ), $custom_link, (int) $total_items, $custom_text, $custom_title);
    }

    return $return;
  }
}
add_filter('bp_notifications_get_notifications_for_user', 'bp_custom_components_format_notif', 10, 5);




// FIRE NOTIFICATION
function bp_cpt_notification($post_id, $post, $update) {

  global $post;
  global $wpdb;

  // Is this a newly created post?
  $is_new = $post->post_date === $post->post_modified;
  if (!$is_new) {
    return;
  }

  $post_type = $post->post_type;

  // Is this a corporate news post or shoutout or alert?
  if ($post_type != ('corporatenews' || 'shoutout' || 'alert')) {
    return;
  }

  // Are BP notifications active?
  if (!bp_is_active('notifications')) {
    return;
  }

  // Should this post send an alert?
  $send_alert = get_post_meta($post_id, 'send_alert', true);
  if (!$send_alert) {
    return;
  }

  // If you said 'yes' to the preceding questions, we can send an alert!
  $alert_type    = get_post_meta($post_id, 'alert_type', true);
  $author_id     = $post->post_author;
  $alerted_users = array();
  $component     = $post_type.'-comp';
  $action        = $post_type.'-posted';

  // Alert all users but the author
  if ($alert_type === 'all') {
    $alerted_users = get_users([
      'exclude' => $author_id,
      'fields' => 'ID'
    ]);

  // Alert individual users
  } elseif ($alert_type === 'choose') {
    $alerted_users = get_post_meta($post_id, 'alert_recipients', true);

  // Alert groups
  } elseif ($alert_type === 'group') {
    $gid_arr = get_post_meta($post_id, 'alert_groups', true);
    $gid_str = implode(',', $gid_arr);
    $grp_users = $wpdb->get_results('SELECT DISTINCT user_id FROM wp_bp_groups_members WHERE group_id IN ('.$gid_str.')', ARRAY_A);
    foreach ($grp_users as $user) {
      $alerted_users[] = $user[user_id];
    }

  // Alert just the author
  } elseif ($alert_type === 'self') {
    $alerted_users[] = $author_id;

  // Alert... nobody I guess
  } else {
    return;
  }

  // Fire the alert
  foreach ($alerted_users as $alerted_user) {
    bp_notifications_add_notification([
      'user_id'           => $alerted_user,
      'item_id'           => $post_id,
      'component_name'    => $component,
      'component_action'  => $action,
      'date_notified'     => bp_core_current_time(),
      'is_new'            => 1
    ]);
  }
}
add_action('save_post', 'bp_cpt_notification', 99, 3);