<?php
/**!
 * Hide Admin topbar for non-admin
 */
add_action('after_setup_theme', 'remove_admin_bar');
	function remove_admin_bar() {
	//if (!current_user_can('administrator') && !is_admin()) {
	  show_admin_bar(false);
	//}
}



/**
 * Block wp-admin access for non-admins
 */
function ace_block_wp_admin() {
	if ( is_admin() && ! current_user_can( 'administrator' ) && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
		wp_safe_redirect( home_url() );
		exit;
	}
}
add_action( 'admin_init', 'ace_block_wp_admin' );

/**
	*Change delete post redirect
*/

add_action( 'parse_request', 'wpse132196_redirect_after_trashing_get' );
function wpse132196_redirect_after_trashing_get() {
    if ( array_key_exists( 'trashed', $_GET ) && $_GET['trashed'] == '1' ) {
        wp_redirect( home_url() );
        exit;
    }
}


/**
	* Custom Login Logo
*/

function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png);
			height:107px;
			width:250px;
			background-size: contain;
			background-repeat: no-repeat;
        	padding-bottom: 0;
        	margin:0 auto;
        	border-bottom:1px solid #d9d9d9;
        }
        
        .login h1 {
	        display: table;
	        margin:auto;
        }
        
        #login h1:after {
	        content:'Company Intranet';
	        font-weight: 500;
	        position: relative;
	        display: table;
	        clear: both;
	        width: auto;
	        margin:10px auto;
	        color:#46b6ba;
        }
        
        .login form {
	        background:#46b6ba!important;
	        color:white!important;
        }
        
        .login label {
	        color:white!important;
        }
        
        .login form .forgetmenot label, .login #backtoblog {
	        display: none;
	        
        }
        
        .login #backtoblog, .login #nav {
	        padding:0!important;
	        text-align: center;
        }
        
        .wp-core-ui .button.button-large {
	        background:#b61218;
	        box-shadow: none;
	        border:none;
	        text-shadow: none;
	        transition:.25s ease-in-out;
	        display: table;
	        clear: both;
	        width:100%;
	        margin-top:10px;
	        
        }
        
        .wp-core-ui .button.button-large:hover {
	        background:red;
        }
        
        .login #login_error, .login .message, .login .success {
	        border-left:4px solid #46b6ba!important;
	        margin-top:10px!important;
	        text-align: center;
        }
        
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );


add_filter( 'login_headerurl', 'custom_loginlogo_url' );
function custom_loginlogo_url($url) {
    return 'http://www.selfdirectinc.com/';
}
