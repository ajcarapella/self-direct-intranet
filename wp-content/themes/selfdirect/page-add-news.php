<?php acf_form_head(); get_header(); ?>
<section id="cms-main">
	<div id="main-contain" class="container-pad">
		
		<?php if( current_user_can('editor') || current_user_can('administrator') ) {  ?>

				<h1>Add News</h1>
				
				<?php
					acf_form(array(
						'post_id'		=> 'new_post',
						'post_content'	=> false,
						'new_post'		=> array(
							'post_type'		=> 'corporatenews',
							'post_status'	=> 'publish'
						),
						'honeypot' => true,
						'submit_value'	=> 'Create News Post',
						'updated_message'   => 'News Post Created!',
						'html_submit_spinner'	=> '<span class="acf-spinner"></span>'
					));
				?>
		<?php } ?>
		
	</div>
	
	
</section>


<?php get_footer(); ?>
