<?php acf_form_head(); get_header(); ?>
<section id="cms-main">
	<div id="main-contain" class="container-pad">
		
		<?php if( current_user_can('editor') || current_user_can('administrator') ) {  ?>

			<?php echo do_shortcode('[gravityform id="1" title="true" description="false" ajax="false"]');?>
		
		<?php } ?>
		
		
				
	</div>
	
	
</section>


<?php get_footer(); ?>
