<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">


	</main><!-- #main -->
</div><!-- #primary -->
<section id="cms-main">
	<div id="main-contain" class="container-pad">
		<h1>Welcome to Self-Direct's Employee Intranet</h1>
		<br><br><br><br>
					<div class="cn-container">
					    <div class="cn-headliner">
					      <i class="far fa-newspaper"></i>  Latest News &amp; Info <i class="fas fa-sync-alt" onClick="window.location.reload()"></i>
				      </div>
				      <div class="cn-article-container">
								<?php
								$loop = new WP_Query( array( 'post_type' => 'corporatenews', 'posts_per_page' => -1 ) );
								while ( $loop->have_posts() ) : $loop->the_post();?>
								
								<div class="cn-article">
									  <span class="cn-date">
									    <?php
									    $postDate = strtotime( $post->post_date );
									    $todaysDate = strtotime( date( 'Y-m-d' ) );
									    $yesterdaysDate = strtotime ('-1 day', $todaysDate);
										$file = get_field('include_file');
									
									    if ( $postDate >= $todaysDate ) {
									      echo 'Today, &nbsp;';
									      echo get_the_time();
									    } elseif ( $postDate >= $yesterdaysDate ) {
									      echo 'Yesterday &nbsp;';
									      echo get_the_time();
									    } else {
									      echo get_the_date("m/d/y");
									    }
									    ?>
									  </span>
									  <span class="cn-headline"><strong><?php the_field('headline');?> <?php if ($file):?>
										&nbsp;-&nbsp;<i class="has-pdf fas fa-file-pdf"></i>
									<?php endif;?></strong> <?php $summary = get_field('details_description'); echo $summary; ?></span>
									  <?php if ( get_field( 'details' ) ): ?>
									    <a class="cn-details" href="<?php the_permalink();?>">View&nbsp;Details&nbsp;></a>
									  <?php else: ?>
									  <?php endif; ?>
									</div>
									
								
								<?php endwhile; wp_reset_query();?>
			
				      </div>
		      	</div>
		    </div>
		  </div><!--row-->
		
	</div>
	
	
</section>

<?php if( is_page('1')):?>
<?php endif; ?>

<?php get_footer(); ?>
