<?php if( have_rows('flex_content') ):?>

	<div class="cms-content-container">
    
    <?php while ( have_rows('flex_content') ) : the_row(); ?>

        <?php if( get_row_layout() == 'headline' ):?>

        	<h2><?php echo the_sub_field('headline');?></h2>

		 <?php elseif( get_row_layout() == 'sub_headline' ): ?>
		 
		 	<h3><?php echo the_sub_field('sub_headline');?></h3>
		 	
		 	
       <?php elseif( get_row_layout() == 'one_column_paragraph' ): ?>

        	<div class="single-column">
	        	<div class="sc-col">
		        	<p><?php echo the_sub_field('text');?></p>
	        	</div>
        	</div>
        	
        <?php elseif( get_row_layout() == 'two_column_paragraph' ): ?>
			<div class="double-column">
				<div class="dc-col">
		        	<p><?php echo the_sub_field('column_one_text');?></p>
				</div>
				
				<div class="dc-col">
		        	<p><?php echo the_sub_field('column_two_text');?></p>
				</div>
			</div>

      <?php elseif( get_row_layout() == 'full_width_image' ): ?>
      		<div class="cms-cc-img">
	      		<img src="<?php echo the_sub_field('image');?>"/>
      		</div>
      		
      		
      <?php elseif( get_row_layout() == 'list_group' ): ?>
		 	<h4><?php echo the_sub_field('list_headline');?></h4>
		 	<?php if( have_rows('list') ): ?>
				 <ul>
				 <?php while( have_rows('list') ): the_row(); 
				 	$item = get_sub_field('list_item');?>
						<li><?php echo $item; ?></li>
					<?php endwhile; ?>
				</ul>	
			<?php endif; ?>
			
			
		<?php elseif( get_row_layout() == 'image_gallery' ): ?>
		
		<div id="flexCarousel" class="carousel slide flex-carousel" data-ride="carousel">
			<div class="carousel-inner">

			<?php if( have_rows('gallery') ): ?>
				<?php while( have_rows('gallery') ): the_row(); 
					$image = get_sub_field('image');
					$label = get_sub_field('label');
				?>
					<div class="carousel-item">
				            <img src="<?php echo $image; ?>" alt="<?php echo $label; ?>"/>
				            <div class="carousel-caption">
				                <h4 class="carousel-headline">
				                   <?php echo $label; ?>
				                </h4>
				            </div>
			        </div>
				<?php endwhile; ?>	
				
					<ul class="carousel-indicators">
					<?php $i = 0; ?>
						<?php while( have_rows('gallery') ): the_row(); ?>
					      	<li data-target="#flexCarousel" data-slide-to="<?php echo $i++;?>" contenteditable="false"></li>
					    <?php endwhile; ?>
				    </ul>
				
					</div>
					
					<div class="carousel-disclaimer">
						Hover to Expand <i class="ml-2 fas fa-expand-arrows-alt"></i>
					</div>
					
				</div>
				
				<script>
					jQuery( '#flexCarousel .carousel-inner').find('.carousel-item:first' ).addClass( 'active' );
					jQuery( '#flexCarousel .carousel-inner .carousel-indicators').find('li:first' ).addClass( 'active' );
				</script>
				
			<?php endif; ?>				

		
			
      <?php elseif( get_row_layout() == 'accordion' ): ?>
	      		
	      		
	      <div id="service-accordion" class="accordion cms-accordion">
		  	<div id="sa-container" class="container-pad">
	      		
	      		<?php if( have_rows('accordion') ): ?>

				
					<div class="sa-ul-container">
						<ul class="sa-ul">
					
						<?php while( have_rows('accordion') ): the_row(); 
					
							// vars
							$title = get_sub_field('acc_title');
							$description = get_sub_field('acc_description');
					
							?>
							
							
							<li class="sa-li">
								<div class="sa--title">
									<?php echo $title; ?>
									<div class="pm-toggle"></div>
								</div>
								<div class="sa--content">
									<p><?php echo $description; ?></p>
								</div>
							</li>
					
					
						<?php endwhile; ?>
			
					</ul>
				</div>
			
			<?php endif; ?>
		  	</div>
	      </div>
      
        <?php endif; endwhile; ?>
    
	</div><!--close cms-content-container-->



<?php endif;?>