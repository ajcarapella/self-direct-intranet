<?php get_header(); ?>
<section id="cms-main">
	<div id="main-contain" class="container-pad">
		<h1><?php echo the_field('headline');?></h1>
		<p><?php echo the_field('details_description');?></p>
		<?php the_content();?>

  </div><!-- /.row -->
</section><!-- /.container-responsive -->

<?php get_footer(); ?>
