<?php acf_form_head(); get_header(); ?>
<section id="cms-main">
	<div id="main-contain" class="container-pad">
		<?php $headline = get_field('headline');?>
		<?php $description = get_field('details_description');?>
		<?php $file = get_field('include_file');?>
		
		<div class="dic-one">
			<h1><?php echo $headline;?> <?php if( current_user_can('editor') || current_user_can('administrator') ) {  ?><div class="delete-form"><a onclick="return confirm('Move this post to the trash?');" href="<?php echo get_delete_post_link($postid); ?>"><i class="fas fa-trash-alt"></i></a></div> <div class="toggle-edit-form"><i class="fas fa-edit"></i></div><?php } ?></h1>
			<p><?php echo $description;?></p>
			<?php the_content();?>
			<?php if ($file):?>
				<a class="pdf-link brand-button" href="<?php echo $file;?>"><i class="fas fa-file-pdf"></i><br><em>PDF Attachment</em></a>
			<?php endif;?>
		</div>

		
		<div class="edit-form-cn">
			<?php acf_form(); ?>
		</div>
		
		
							
								



			<?php if(!is_front_page()):?>
				<a class="back-button" href="<?php echo home_url('/'); ?>">&laquo; Go Back</a>
			<?php endif; ?>

  </div><!-- /.row -->
</section><!-- /.container-responsive -->
<script>
	jQuery('.toggle-edit-form, .toggle-view-form').click(function() {
			jQuery('.toggle-edit-form').toggle();
			jQuery('.toggle-view-form').toggle();
			jQuery('.edit-form-cn').toggle();
			jQuery('.dic-one').toggle();
		});
</script>
<?php get_footer(); ?>
