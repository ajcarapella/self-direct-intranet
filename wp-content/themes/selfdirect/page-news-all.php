<?php get_header(); ?>
<section id="cms-main">
	<div id="main-contain" class="container-pad">
					<div class="cn-container">
					    <div class="cn-headliner">
					      <i class="far fa-newspaper"></i> All News &amp; Info <i class="fas fa-sync-alt" onClick="window.location.reload()"></i> <a href="<?php echo home_url('/'); ?>corporate-news-all" class="cn-view-all">View Full Page ></a>
				      </div>
				      <div class="cn-article-container">
								<?php
								$loop = new WP_Query( array( 'post_type' => 'corporatenews', 'posts_per_page' => 8 ) );
								while ( $loop->have_posts() ) : $loop->the_post();?>
								
								<div class="cn-article">
									
									  <span class="cn-date">
									    <?php
									    $postDate = strtotime( $post->post_date );
									    $todaysDate = strtotime( date( 'Y-m-d' ) );
									    $yesterdaysDate = strtotime ('-1 day', $todaysDate);
										$file = get_field('include_file');
									
									    if ( $postDate >= $todaysDate ) {
									      echo 'Today, &nbsp;';
									      echo get_the_time();
									    } elseif ( $postDate >= $yesterdaysDate ) {
									      echo 'Yesterday &nbsp;';
									      echo get_the_time();
									    } else {
									      echo get_the_date("m/d/y");
									    }
									    ?>
									  </span>
									  <?php if ($file):?>
										<span class="bullet">&nbsp;•&nbsp;</span><i class="has-pdf fas fa-file-pdf"></i>
									<?php endif;?>
									  <span class="bullet">&nbsp;•&nbsp;</span>
									  <span class="cn-headline"><strong><?php the_field('headline');?> -</strong> <?php $summary = get_field('details_description'); echo $summary; ?></span>
									  <?php if ( get_field( 'details' ) ): ?>
									    <a class="cn-details" href="<?php the_permalink();?>">View&nbsp;Details&nbsp;></a>
									  <?php else: ?>
									  <?php endif; ?>
									</div>
								
								<?php endwhile; wp_reset_query();?>
			
				      </div>
		      	</div>
		    </div>
		  </div><!--row-->
		
	</div>
	
	
</section>


<?php get_footer(); ?>
